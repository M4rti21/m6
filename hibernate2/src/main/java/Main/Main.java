package Main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Models.Aliment;
import Models.Etapa;
import Models.Joguina;
import Models.Tamagochi;

public class Main {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		try {
			if (sessionFactory == null) {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
						.configure("hibernate.cfg.xml").build();
				Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String[] args) {
		// OPEN
		session = getSessionFactory().openSession();
		session.beginTransaction();

		Tamagochi t1 = new Tamagochi("Marti", "Hola", Etapa.Baby);
		Tamagochi t2 = new Tamagochi("Roger", "Adeu", Etapa.Baby);
		Tamagochi t3 = new Tamagochi("Arlet", "Qtal", Etapa.Baby);
		Tamagochi t4 = new Tamagochi("David", "MltB", Etapa.Baby);

		Aliment a1 = new Aliment("Poma", "si", 2.2);
		Aliment a2 = new Aliment("Xocoloata", "no", 4.4);
		Aliment a3 = new Aliment("Pera", "potser", 6.6);
		Aliment a4 = new Aliment("Pollastre", "depen", 8.8);

		// INSERT
		session.persist(a1);
		session.persist(a2);
		session.persist(a3);
		session.persist(a4);

		session.persist(t1);
		session.persist(t2);
		session.persist(t3);
		session.persist(t4);

		// SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		t1.setAmic(t2);
		t2.setAmic(t1);
		t3.setAmic(t4);
		t4.setAmic(t3);

		// UPDATE
		session.merge(t1);
		session.merge(t2);
		session.merge(t3);
		session.merge(t4);

		// SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		// GET ALL
		@SuppressWarnings("unchecked")
		List<Tamagochi> db_ts1 = session.createQuery("from Tamagochi").getResultList();
		System.out.println("----TAMAGOCHIS DB----");
		for (Tamagochi t : db_ts1) {
			System.out.println(t);
		}

		Tamagochi db_t1 = session.find(Tamagochi.class, 1);
		db_t1.setAliment(a1);
		Tamagochi db_t2 = session.find(Tamagochi.class, 2);
		db_t2.setAliment(a2);
		Tamagochi db_t3 = session.find(Tamagochi.class, 3);
		db_t3.setAliment(a2);

		// UPDATE
		session.merge(db_t1);
		session.merge(db_t2);
		session.merge(db_t3);

		// SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		@SuppressWarnings("unchecked")
		List<Tamagochi> db_ts2 = session.createQuery("from Tamagochi").getResultList();
		System.out.println("----TAMAGOCHIS DB----");
		for (Tamagochi t : db_ts2) {
			System.out.print(t.getNom() + ": ");
			if (t.getAliment() != null) {
				System.out.print(t.getAliment().getNom());
			}
			System.out.println();
		}

		Joguina j1 = new Joguina("Pilota", "si", 2);
		Joguina j2 = new Joguina("hotwheels", "no", 4);

		// INSERT
		session.persist(j1);
		session.persist(j2);

		t1.setJoguina(j1);
		t2.setJoguina(j2);

		// UPDATE
		session.merge(t1);
		session.merge(t2);

		// SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		try {
			Tamagochi db_t1_2 = session.find(Tamagochi.class, 1);
			db_t1_2.setJoguina(j2);

			session.merge(db_t1_2);

			// SAVE
			session.getTransaction().commit();
			session.getTransaction().begin();
		} catch (Exception e) {
			
		}
		
		@SuppressWarnings("unchecked")
		List<Tamagochi> db_ts3 = session.createQuery("from Tamagochi").getResultList();
		System.out.println("----TAMAGOCHIS DB----");
		for (Tamagochi t : db_ts3) {
			System.out.print(t.getNom() + ": ");
			if (t.getAliment() != null) {
				System.out.print(t.getAliment().getNom() + " - ");
			}
			if (t.getJoguina() != null) {
				System.out.print(t.getJoguina().getNom());
			}
			System.out.println();
		}
		
		// CLOSE
		session.close();
	}
}
