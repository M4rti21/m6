package Models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tamagochis")
public class Tamagochi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String descripcio;
	private double gana;
	private boolean viu;
	private int felicitat;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Etapa etapa;
	private LocalDateTime data_naixement;
	@ManyToOne
	@JoinColumn(name = "id_aliment")
	private Aliment aliment;
	@OneToOne
	@JoinColumn(name = "id_joguina", unique = true)
	private Joguina joguina;
	@OneToOne
	@JoinColumn(name = "id_amic", unique = true)
	private Tamagochi amic;

	public int getId() {
		return id;
	}

	public Tamagochi() {
	}

	public Tamagochi(String nom, String descripcio, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
	}

	public Tamagochi(int id, String nom, String descripcio, double gana, boolean viu, int felicitat, Etapa etapa,
			LocalDateTime data_naixement, Aliment aliment, Joguina joguina) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.data_naixement = data_naixement;
		this.aliment = aliment;
		this.joguina = joguina;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getData_naixement() {
		return data_naixement;
	}

	public void setData_naixement(LocalDateTime data_naixement) {
		this.data_naixement = data_naixement;
	}

	public Aliment getAliment() {
		return aliment;
	}

	public void setAliment(Aliment aliment) {
		this.aliment = aliment;
	}

	public Tamagochi getAmic() {
		return amic;
	}

	public void setAmic(Tamagochi amic) {
		this.amic = amic;
	}

	public Joguina getJoguina() {
		return joguina;
	}

	public void setJoguina(Joguina joguina) {
		this.joguina = joguina;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Tamagochi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu=" + viu
				+ ", felicitat=" + felicitat + ", etapa=" + etapa + ", data_naixement=" + data_naixement + ", aliment="
				+ aliment + ", joguina=" + joguina + "]";
	}

}