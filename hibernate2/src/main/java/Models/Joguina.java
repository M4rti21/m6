package Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "joguines")
public class Joguina {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 50, nullable = false)
	private String nom;
	@Column(length = 100, nullable = false)
	private String descripcio;
	private int nivell_diversio = 5;
	@OneToOne(mappedBy = "joguina")
	private Tamagochi tamagochi;

	public Joguina() {
	}

	public Joguina(String nom, String descripcio, int nivell_diversio) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.nivell_diversio = nivell_diversio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public int getNivell_diversio() {
		return nivell_diversio;
	}

	public void setNivell_diversio(int nivell_diversio) {
		this.nivell_diversio = nivell_diversio;
	}

	@Override
	public String toString() {
		return "Joguina [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", nivell_diversio="
				+ nivell_diversio + "]";
	}

}
