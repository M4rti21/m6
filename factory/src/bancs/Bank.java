package bancs;

import java.util.ArrayList;
import java.util.List;

public class Bank {
	private String name;
	private List<Account> accounts;
	
	public Bank(String n) {
		this.name = n;
		this.accounts = new ArrayList<Account>();
	}
	
	public void addAccount(Account a) {
		accounts.add(a);
	}

	public String getName() {
		return name;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	@Override
	public String toString() {
		return "Bank [name=" + name + ", accounts=" + accounts + "]";
	}
	
}
