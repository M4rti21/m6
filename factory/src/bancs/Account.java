package bancs;

import java.util.Random;

import main.Client;

public class Account {
	Client client;
	int codi;
	AccountType type;
	
	public Account(Client client, AccountType type) {
		Random r = new Random();
		this.client = client;
		this.codi =  r.nextInt(10000000, 100000000);
		this.type = type;
	}

	@Override
	public String toString() {
		return "Account [client=" + client + ", codi=" + codi + "]";
	}
	
}
