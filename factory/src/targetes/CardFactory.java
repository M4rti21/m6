package targetes;

import bancs.AccountType;
import bancs.Bank;

public class CardFactory {	
	private FactoryEuro6000 fe = new FactoryEuro6000();
	private Factory4B f4 = new Factory4B();
	private FactoryAmericanExpress fa = new FactoryAmericanExpress();
	private FactoryMastercard fm = new FactoryMastercard();
	private FactoryServired fs = new FactoryServired();
	private FactoryVisa fv = new FactoryVisa();
	
	public Card makeCard(CardType t, CardBrand b, Bank k) {
		switch (b) {
		case EURO6000:
			return fe.makeCard(t, k);
		case _4B:
			return f4.makeCard(t, k);
		case AMERICAN_EXPRESS:
			return fa.makeCard(t, k);
		case MASTERCARD:
			return fm.makeCard(t, k);
		case SERVIRED:
			return fs.makeCard(t, k);
		case VISA:
			return fv.makeCard(t, k);
		default:
			return null;
		}
	}
	
}
