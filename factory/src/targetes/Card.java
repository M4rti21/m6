package targetes;

import bancs.Bank;

public class Card {
	private Bank bank;
	private CardType tipus;
	private CardBrand brand;
	private int codi;
	
	public Card(CardType t, CardBrand b, int c, Bank k) {
		this.tipus = t;
		this.brand = b;
		this.codi = c;
		this.bank = k;
	}

	@Override
	public String toString() {
		return "Card [bank=" + bank + ", tipus=" + tipus + ", brand=" + brand + ", codi=" + codi + "]";
	}
	
}
