package targetes;

import java.util.Random;

import bancs.Bank;

public abstract class Factory {
	private static final Random r = new Random();
	CardBrand brand;
	
	public Factory(CardBrand b) {
		this.brand = b;
	}
	
	public Card makeCard(CardType t, Bank k) {
		return new Card(t, this.brand, r.nextInt(10000000, 100000000), k);
	}
}
