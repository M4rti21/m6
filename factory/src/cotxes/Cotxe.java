package cotxes;

public abstract class Cotxe {
	Model model;
	Localitzacio localitzacio;
	
	public Cotxe(Model m, Localitzacio l) {
		this.model = m;
		this.localitzacio = l;
	}

	@Override
	public String toString() {
		return "Cotxe [model=" + model + ", localitzacio=" + localitzacio + "]";
	}
	
}
