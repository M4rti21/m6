package cotxes;

public abstract class Factory {
	Localitzacio localitzacio;

	public Factory(Localitzacio l) {
		this.localitzacio = l;
	}

	public Cotxe newCar(Model m) {
		switch (m) {
		case ELECTRIC:
			return new CotxeElectric(this.localitzacio);
		case DRAC:
			return new CotxeDrac(this.localitzacio);
		case HIBRID:
			return new CotxeHibrid(this.localitzacio);
		case HIDROGEN:
			return new CotxeHidrogen(this.localitzacio);
		case BIFUEL_I_CAVALLS:
			return new CotxeBifuel(this.localitzacio);
		default:
			return null;
		}
	}
}
