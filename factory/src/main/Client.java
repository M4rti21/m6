package main;

import targetes.Card;

public class Client {
	private String nom;
	private int salari;
	private Card targeta;
	
	public Client(String n, int s) {
		this.nom = n;
		this.salari = s;
	}
	
	public Client(String n, int s, Card t) {
		this.nom = n;
		this.salari = s;
		this.targeta = t;
	}

	public String getNom() {
		return nom;
	}

	public int getSalari() {
		return salari;
	}


	public Card getTargeta() {
		return targeta;
	}

	public void setTargeta(Card targeta) {
		this.targeta = targeta;
	}

	@Override
	public String toString() {
		return "Client [nom=" + nom + ", salari=" + salari + ", targeta=" + targeta + "]";
	}
	
}
