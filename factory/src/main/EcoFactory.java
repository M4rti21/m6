package main;

import cotxes.Cotxe;
import cotxes.FactoryDorne;
import cotxes.FactoryHarrenhal;
import cotxes.FactoryInvernalia;
import cotxes.FactoryRoca;
import cotxes.Localitzacio;
import cotxes.Model;

public class EcoFactory {
	private FactoryInvernalia fi = new FactoryInvernalia();
	private FactoryRoca fr = new FactoryRoca();
	private FactoryHarrenhal fh = new FactoryHarrenhal();
	private FactoryDorne fd = new FactoryDorne();
	
	public Cotxe newCotxe(Model m, Localitzacio l) {
		switch(l) {
		case INVERNALIA:
			return fi.newCar(m);
		case ROCA_CASTERLY:
			return fr.newCar(m);
		case HARRENHAL:
			return fh.newCar(m);
		case EL_PRINCIPAT_DE_DORNE_I_TEMPESTES:
			return fd.newCar(m);
		}
		return null;
	}
}
