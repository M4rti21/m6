package main;

public interface Aniversari {
	int getAny();

	Mes getMes();

	int getDia();

	boolean isLaterThan(Aniversari other);

	boolean isSame(Aniversari other);

	public enum Mes {
		JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
	}
}
