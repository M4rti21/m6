package main;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarAdapter implements Aniversari {
	GregorianCalendar calendar;
	
	public CalendarAdapter(int year, Aniversari.Mes month, int day) {
		super();
		this.calendar = new GregorianCalendar(year, month.ordinal(), day);
	}

	@Override
	public int getAny() {
		return this.calendar.get(Calendar.YEAR);
	}

	@Override
	public Mes getMes() {
		return Mes.values()[calendar.get(Calendar.MONTH)];
	}

	@Override
	public int getDia() {
		return this.calendar.get(Calendar.DAY_OF_MONTH);
	}

	@Override
	public boolean isLaterThan(Aniversari other) {
		if (isSame(other)) return false;
		if (this.getAny() > other.getAny()) return true;
		if (this.getAny() < other.getAny()) return false;
		if (this.getMes().ordinal() > other.getMes().ordinal()) return true;
		if (this.getMes().ordinal() < other.getMes().ordinal()) return false;
		if (this.getDia() > other.getDia()) return true;
		if (this.getDia() < other.getDia()) return false;
		return false;
	}

	@Override
	public boolean isSame(Aniversari other) {
		if (this.getMes() != other.getMes()) return false;
		if (this.getMes() != other.getMes()) return false;
		if (this.getDia() != other.getDia()) return false;
		if (this.getDia() != other.getDia()) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.getDia() + "/" + (this.getMes().ordinal() + 1) + "/" + this.getAny();
	}

}
