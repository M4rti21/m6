package main;

public class FactoryAniversari {
	public Aniversari getAniversari(int year, Aniversari.Mes month, int day) {
		return new CalendarAdapter(year, month, day);
	};
}
