package main;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PowerRanger implements PropertyChangeListener, HeroiEquipat {
	private String nom;
	private String nomMecha;
	private Colors color;
	private double poder;

	public PowerRanger(String nom, String nomMecha, Colors color, double poder) {
		super();
		this.nom = nom;
		this.nomMecha = nomMecha;
		this.color = color;
		this.poder = poder;
	}

	public String getNom() {
		return nom;
	}

	public String getNomMecha() {
		return nomMecha;
	}

	public Colors getColor() {
		return color;
	}

	public double getPoder() {
		return poder;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String e = evt.getPropertyName();
		if (e.equals("new enemic")) {
			handle_new_enemic(evt);
		}
		if (e.equals("attack")) {
			handle_attack(evt);
		}

	}

	private void handle_attack(PropertyChangeEvent evt) {
		Enemic e = (Enemic) evt.getNewValue();
		System.out.println(nom + ": " + e.getNom() + " ha attacat");
		System.out.println(equipar());
	}

	private void handle_new_enemic(PropertyChangeEvent evt) {
		Enemic e = (Enemic) evt.getNewValue();
		System.out.println(nom + ": AAAAAA, un nou enemic");
		if (e.getNemesis() != this)
			return;
		e.addListener(this);
		System.out.println(nom + ": Estic vigilant a " + e.getNom());
	}

	@Override
	public String toString() {
		return "PowerRanger [nom=" + nom + ", nomMecha=" + nomMecha + ", color=" + color + ", poder=" + poder + "]";
	}

	@Override
	public String equipar() {
		
		Arma a = new Arma(this);
		Mecha m = new Mecha(this);
		
		ArmaAncestralAdapter aa = new ArmaAncestralAdapter(this, new ArmaAncestral());
		
		System.out.println(a.equipar());
		System.out.println(m.equipar());
		System.out.println(aa.equipar());
		return "EL MEU PODER ES " + this.getPoder();
	}

	@Override
	public void setPoder(double p) {
		this.poder = p;
	}

}
