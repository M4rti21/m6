package main;

public class Mecha extends DecoratorHeroi {

	private String nom;

	public Mecha(HeroiEquipat heroi) {
		super(heroi);
		this.nom = heroi.getNomMecha();
	}

	@Override
	public String equipar() {
		this.setPoder(getPoder());
		return "SOC EL MECHA " + this.nom + " I EL MEU PODER ES " + getPoder();
	}

	@Override
	public void setPoder(double p) {
		this.heroi.setPoder(p * 2);
	}

}
