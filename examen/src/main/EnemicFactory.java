package main;

import java.beans.PropertyChangeSupport;

public class EnemicFactory {
	private PropertyChangeSupport support;

	public EnemicFactory() {
		this.support = new PropertyChangeSupport(this);
	}
	
	public Enemic generate(TipusEnemic t, String nom, PowerRanger nemesis) {
		Enemic e;
		switch (t) {
		case ZED: {
			e = new LordZedd(nom, nemesis);
			break;
		}
		case RITA:
			e = new RitaRepulsa(nom, nemesis);
			break;
		default:
			e = null;
		}
		this.support.firePropertyChange("new enemic", null, e);
		return e;
	}

	public void addListener(PowerRanger u) {
		this.support.addPropertyChangeListener(u);
	}

	public void delListener(PowerRanger u) {
		this.support.removePropertyChangeListener(u);
	}

}
