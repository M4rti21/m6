package main;

public interface Enemic{
	public String getNom();

	public PowerRanger getNemesis();

	public void atacar();
	
	public void addListener(PowerRanger u);

	public void delListener(PowerRanger u);
}
