package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DBHandler {

	private String db_conn;

	private Connection conn;
	private Statement stmt;
	private ResultSet rs;

	public DBHandler(String db_host, String db_name, String db_user, String db_pass) {
		super();

		conn = null;
		stmt = null;
		rs = null;

		db_conn = "jdbc:mysql://" + db_host + "/" + db_name + "?" + "user=" + db_user + "&password=" + db_pass;
	}

	public boolean connect() {
		try {
			conn = DriverManager.getConnection(db_conn);
			stmt = conn.createStatement();
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	public void close() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException sqlEx) {
			}
			rs = null;
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
			}
			stmt = null;
		}
	}

	public List<Missio> get_missions() {
		List<Missio> missions = new ArrayList<Missio>();
		try {
			if (stmt.execute("SELECT * FROM missions")) {
				rs = stmt.getResultSet();
				if (rs == null)
					return missions;

				while (rs.next()) {
					int id = rs.getInt("idmissions");
					String nomEnemic = rs.getString("nomEnemic");
					String descripcio = rs.getString("descripcio");
					int urgencia = rs.getInt("urgencia");
					Missio m = new Missio(id, nomEnemic, descripcio, urgencia);
					missions.add(m);
				}
			}
		} catch (SQLException e) {
			sql_exception(e);
		}
		return missions;
	}

	public boolean write_missio(Missio m) {
		String insert = "INSERT INTO missions (idmissions, descripcio, nomEnemic, urgencia)";
		Map<String, String> i = m.getInfo();
		try {
			String values = " VALUES ('" + i.get("descripcio") + "', '" + i.get("nomEnemic") + "', '"
					+ i.get("urgencia") + "')";
			String handler = " ON DUPLICATE KEY UPDATE idmissions=" + i.get("id") + ", descripcio='" + i.get("descripcio") + "', nomEnemicA='"
					+ i.get("nomEnemic")+ "', urgencia='" + i.get("urgencia") + "';";
			String sql = insert + values + handler;
			stmt.execute(sql);
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}
	
	public boolean update_missio(Missio m) {
		String insert = "UPDATE missions";
		Map<String, String> i = m.getInfo();
		try {
			String values = " SET urgencia='"+ i.get("urgencia") + "' WHERE idmissions="+i.get("id");
			String sql = insert + values;
			stmt.execute(sql);
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	public boolean delete_missio(Missio m) {
		String sql = "DELETE FROM missions WHERE idmissions=" + m.getInfo().get("id");
		try {
			stmt.execute(sql);
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	private void sql_exception(SQLException e) {
		System.out.println("SQLException: " + e.getMessage());
		System.out.println("SQLState: " + e.getSQLState());
		System.out.println("VendorError: " + e.getErrorCode());
	}

}
