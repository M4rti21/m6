package main;

public class ArmaAncestralAdapter extends DecoratorHeroi implements HeroiEquipat  {
	
	ArmaAncestral arma;
	
	public ArmaAncestralAdapter(HeroiEquipat heroi, ArmaAncestral arma) {
		super(heroi);
		this.arma = arma;
	}

	@Override
	public String equipar() {
		this.setPoder(super.getPoder());
		System.out.println("EXTRA DE PODER ANTIC!!!");
		return arma.alliberarPoder();
	}

	@Override
	public void setPoder(double p) {
		this.heroi.setPoder(p + 11111);
	}

}
