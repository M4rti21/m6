package main;

import java.beans.PropertyChangeSupport;

public class RitaRepulsa implements Enemic {
	private String nom;
	private PowerRanger nemesis;
	
	private PropertyChangeSupport support;


	public RitaRepulsa(String nom, PowerRanger nemesis) {
		super();
		this.nom = nom;
		this.nemesis = nemesis;
		this.support = new PropertyChangeSupport(this);
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public PowerRanger getNemesis() {
		return nemesis;
	}

	@Override
	public void atacar() {
		this.support.firePropertyChange("attack", null, this);
	}

	@Override
	public String toString() {
		return "RitaRepulsa [nom=" + nom + ", nemesis=" + nemesis + "]";
	}
	
	public void addListener(PowerRanger u) {
		this.support.addPropertyChangeListener(u);
	}

	public void delListener(PowerRanger u) {
		this.support.removePropertyChangeListener(u);
	}

}
