package main;

import java.util.LinkedHashMap;
import java.util.Map;

public class Missio {
	
	private Map<String, String> info;
	
	public Missio(int id, String nomEnemic, String descripcio, int urgencia) {
		super();
		info = new LinkedHashMap<String, String>();
		info.put("id", id + "");
		info.put("nomEnemic", nomEnemic);
		info.put("descripcio", descripcio);
		info.put("urgencia", urgencia + "");
	}

	public Map<String, String> getInfo() {
		return info;
	}

	public void setInfo(Map<String, String> info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return info.toString();
	}
	
	
}
