package main;

public abstract class DecoratorHeroi implements HeroiEquipat {
	public HeroiEquipat heroi;
	
	public DecoratorHeroi(HeroiEquipat heroi){
		this.heroi = heroi;
	}
	
	@Override
	public double getPoder() {
		return heroi.getPoder();
	}
	
	@Override
	public String getNomMecha() {
		return this.heroi.getNomMecha();
	}

}
