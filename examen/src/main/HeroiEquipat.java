package main;

public interface HeroiEquipat {
	public String equipar();
	public double getPoder();
	public void setPoder(double p);
	public String getNomMecha();
}
