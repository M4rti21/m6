package main;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class Main {

	public static void main(String[] args) {
		ex_1_2();
		//ex_3_4_5();
	}

	private static void ex_1_2() {
		
		Comparator<Missio> sort_missions = (m2, m1) -> m2.getInfo().get("urgencia").compareTo(m1.getInfo().get("urgencia"));
		
		Queue<Missio> missions = new PriorityQueue<Missio>(sort_missions);

		missions.offer(new Missio(1, "pep", "wtf perque", 3));
		missions.offer(new Missio(2, "pepa", "les missions", 4));
		missions.offer(new Missio(3, "pepet", "son maps???", 1));

		print_missions(missions);

		missions.offer(new Missio(4, "pepeta", "no entenc res", 5));
		
		print_missions(missions);

//		String db_host = "localhost";
//		String db_name = "powerchamber";
//		String db_user = "root";
//		String db_pass = "super3";
//		DBHandler db = new DBHandler(db_host, db_name, db_user, db_pass);
//
//		if (!db.connect())
//			return;
//
//		Queue<Missio> db_missions = new PriorityQueue<Missio>(sort_missions);
//
//		List<Missio> db_read_missions = db.get_missions();
//		for (Missio missio : db_read_missions) {
//			db_missions.offer(missio);
//		}
//
//		print_missions(db_missions);
//
//		for (Missio m : db_read_missions) {
//			if (m.getInfo().get("descripcio") == null) {
//				db.delete_missio(m);
//			}
//		}
//
//		db_missions = new PriorityQueue<Missio>(sort_missions);
//
//		db_read_missions = db.get_missions();
//		for (Missio m : db_read_missions) {
//			db_missions.offer(m);
//		}
//
//		for (Missio m : db_read_missions) {
//			if (m.getInfo().get("nomEnemic") == null) {
//				db.update_missio(new Missio(Integer.parseInt(m.getInfo().get("id")), m.getInfo().get("nomEneimc"),
//						m.getInfo().get("descripcio"), 4));
//			}
//		}
//
//		db_missions = new PriorityQueue<Missio>(sort_missions);
//
//		db_read_missions = db.get_missions();
//		for (Missio m : db_read_missions) {
//			db_missions.offer(m);
//		}
//
//		print_missions(db_missions);
//
//		db.close();
	}

	private static void ex_3_4_5() {
		PowerRanger red = new PowerRanger("pep", "pepet", Colors.VERMELL, 7.27);
		PowerRanger yellow = new PowerRanger("pepa", "pepeta", Colors.GROC, 4.20);

		EnemicFactory ef = new EnemicFactory();

		ef.addListener(red);
		ef.addListener(yellow);

		System.out.println("Creem un enemic");
		Enemic e1 = ef.generate(TipusEnemic.ZED, "pepe", red);
		System.out.println();
		System.out.println("Creem un altre enemic");
		Enemic e2 = ef.generate(TipusEnemic.ZED, "papa", yellow);
		System.out.println();
		System.out.println(e1);
		System.out.println(e2);

		e1.atacar();
		System.out.println();
		e2.atacar();
		
		System.out.println(red.equipar());
	}

	private static void print_missions(Queue<Missio> ms) {
		System.out.println("MISSIONS:");
		while (!ms.isEmpty()) {
			System.out.println(ms.poll());
		}
	}
}
