package main;

public class Arma extends DecoratorHeroi {

	public Arma(HeroiEquipat heroi) {
		super(heroi);
	}

	@Override
	public String equipar() {
		this.setPoder(getPoder());
		return "ME EQUIPAT UNA ARMA I EL MEU PODER ES " + getPoder();
	}

	@Override
	public void setPoder(double p) {
		this.heroi.setPoder(p + 2.5);
	}

}
