package Models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tamagochis")
public class Tamagochi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String descripcio;
	private double gana;
	private boolean viu;
	private int felicitat;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Etapa etapa;
	private LocalDateTime data_naixement;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_aliment")
	private Aliment aliment;
	@OneToOne
	@JoinColumn(name = "id_joguina", unique = true)
	private Joguina joguina;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinColumn(unique = true)
	private List<Tamagochi> amics = new ArrayList<Tamagochi>();
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "amics")
	private List<Tamagochi> amics_de = new ArrayList<Tamagochi>();
	
	public int getId() {
		return id;
	}

	public Tamagochi() {
	}

	public Tamagochi(String nom, String descripcio, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
	}
	
	public Tamagochi(String nom, String descripcio, Etapa etapa, double gana) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
		this.gana = gana;
	}

	public Tamagochi(int id, String nom, String descripcio, double gana, boolean viu, int felicitat, Etapa etapa,
			LocalDateTime data_naixement, Aliment aliment, Joguina joguina) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.data_naixement = data_naixement;
		this.aliment = aliment;
		this.joguina = joguina;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public List<Tamagochi> getAmics_de() {
		return amics_de;
	}

	public void setAmics_de(List<Tamagochi> amics_de) {
		this.amics_de = amics_de;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getData_naixement() {
		return data_naixement;
	}

	public void setData_naixement(LocalDateTime data_naixement) {
		this.data_naixement = data_naixement;
	}

	public Aliment getAliment() {
		return aliment;
	}

	public void setAliment(Aliment aliment) {
		this.aliment = aliment;
	}

	public List<Tamagochi> getAmics() {
		return amics;
	}

	public void setAmics(List<Tamagochi> amics) {
		this.amics = amics;
	}

	public Joguina getJoguina() {
		return joguina;
	}

	public void setJoguina(Joguina joguina) {
		this.joguina = joguina;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Tamagochi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu=" + viu
				+ ", felicitat=" + felicitat + ", etapa=" + etapa + ", data_naixement=" + data_naixement + ", aliment="
				+ aliment + ", joguina=" + joguina + "]";
	}
	
	public String getAmicsString() {
		String a = "";
		for (Tamagochi amic : amics) {
			a += amic.getNom() + ", ";
		}
		return a;
	}
	
	public String toCoolString() {
		return nom + ": " + descripcio + "| amics: " + getAmicsString() +" | gana=" + gana + ", viu=" + viu
				+ ", felicitat=" + felicitat + ", etapa=" + etapa + ", data_naixement=" + data_naixement + ", aliment="
				+ aliment + ", joguina=" + joguina;
	}

}