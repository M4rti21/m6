package Models;

public class DAOJoguina extends DAOGeneric<Joguina, Integer> {

	public DAOJoguina() {
		super(Joguina.class);
	}

	public void generateJoguina(String nom, String descripcio, int diversio) {
		Joguina j = new Joguina(nom, descripcio, diversio);
		this.save(j);
	}
	
	public void updateDiversio(int id, int diversio) {
		Joguina j = this.find(id);
		j.setNivell_diversio(diversio);
		this.update(j);
	}

}
