package Models;

public class DAOAliment extends DAOGeneric<Aliment, Integer> {
	
	public DAOAliment() {
		super(Aliment.class);
	}

	public void generateAliment(String nom, String descripcio, double valor_nutricional) {
		Aliment a = new Aliment(nom, descripcio, valor_nutricional);
		this.save(a);
	}
	
	public void listTamagochis(int id) {
		Aliment a = this.find(id);
		System.out.println("Tamagochis amb l'aliment " + a.getNom());
		a.getTamagochis().iterator().forEachRemaining(t -> System.out.println("- " + t.getNom()));
	}
	
	public void updateValorNutricional(int id, int valor_nutricional) {
		Aliment a = this.find(id);
		a.setValor_nutricional(valor_nutricional);
		this.update(a);
	}

}
