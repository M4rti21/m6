package Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "aliments")
public class Aliment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length=50, nullable = false)
	private String nom;
	@Column(length=50, nullable = false)
	private String descripcio;
	@Column(columnDefinition = "double(6,2)")
	private double valor_nutricional = 10;
	@OneToMany(mappedBy = "aliment", fetch = FetchType.EAGER)
	private Set<Tamagochi> tamagochis = new HashSet<Tamagochi>();
	
	public Aliment() {
	}

	public Aliment(String nom, String descripcio, double valor_nutricional) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.valor_nutricional = valor_nutricional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getValor_nutricional() {
		return valor_nutricional;
	}

	public void setValor_nutricional(double valor_nutricional) {
		this.valor_nutricional = valor_nutricional;
	}
	
	public Set<Tamagochi> getTamagochis() {
		return this.tamagochis;
	}

	@Override
	public String toString() {
		return "Aliment [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", valor_nutricional="
				+ valor_nutricional + ", tamagochis=" + tamagochis + "]";
	}

}
