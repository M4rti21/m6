package Models;

import java.util.List;

public class DAOTamagochi extends DAOGeneric<Tamagochi, Integer> {

	public DAOTamagochi() {
		super(Tamagochi.class);
	}

	public void generateTamagochi(String nom, String descripcio, Etapa etapa) {
		Tamagochi t = new Tamagochi(nom, descripcio, etapa);
		this.save(t);
	}
	
	public Tamagochi generateTamagochi(String nom, String descripcio, Etapa etapa, double gana) {
		Tamagochi t = new Tamagochi(nom, descripcio, etapa, gana);
		this.save(t);
		return t;
	}
	
	public void equiparJoguina(int id, Joguina j) {
		Tamagochi t = this.find(id);
		t.setJoguina(j);
		this.update(t);
	}
	
	public void equiparAliment(int id, Aliment a) {
		Tamagochi t = this.find(id);
		t.setAliment(a);
		this.update(t);
	}
	
	public void equipar(Tamagochi t, Aliment a) {
		t.setAliment(a);
		this.update(t);
	}
	
	public void equipar(Tamagochi t, Joguina j) {
		t.setJoguina(j);
		this.update(t);
	}
	
	public boolean isViu(Tamagochi t) {
		return t.isViu();
	}
	
	public void utilitzarJoguina(Tamagochi t) {
		int diversio = t.getJoguina().getNivell_diversio();
		t.setFelicitat(t.getFelicitat() + diversio);
		t.setJoguina(null);
		this.update(t);
	}
	
	public void alimentarse(Tamagochi t) {
		double valor = t.getAliment().getValor_nutricional();
		t.setGana(Math.max(t.getGana() - valor, 0));
		t.setAliment(null);
		this.update(t);
	}
	
	public void ferseAmics(Tamagochi t1, Tamagochi t2) {
		List<Tamagochi> t1_amics = t1.getAmics();
		t1_amics.add(t2);
		List<Tamagochi> t2_amics = t2.getAmics();
		t2_amics.add(t1);
		
		this.update(t1);
	}
	
	public void llistarAmics(Tamagochi t) {
		System.out.println("Amics de " + t.getNom());
		t.getAmics().forEach(a -> System.out.println("- " + a.getNom()));
	}
	
	public void checkGana(Tamagochi t) {
		if (t.getGana() > 200) {
			if (t.getAliment() == null) {
				t.setViu(false);
				this.update(t);
			} else {
				alimentarse(t);
			}
		}
	}

}
