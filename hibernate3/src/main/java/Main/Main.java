package Main;

import java.util.List;

import Models.Aliment;
import Models.DAOAliment;
import Models.DAOJoguina;
import Models.DAOTamagochi;
import Models.Etapa;
import Models.Joguina;
import Models.Tamagochi;
import Models.Utils;

@SuppressWarnings("unused")
public class Main {

	public static void main(String[] args) {

		DAOTamagochi dt = new DAOTamagochi();
		DAOAliment da = new DAOAliment();
		DAOJoguina dj = new DAOJoguina();

		Tamagochi t1 = dt.generateTamagochi("Marti", "Hola", Etapa.Baby, 100);
		
		dt.generateTamagochi("Roger", "Adeu", Etapa.Baby);
		dt.generateTamagochi("Arlet", "Qtal", Etapa.Baby);
		dt.generateTamagochi("David", "MltB", Etapa.Baby);
		
		da.generateAliment("Poma", "si", 2.2);
		da.generateAliment("Xocoloata", "no", 4.4);
		da.generateAliment("Pera", "potser", 6.6);
		da.generateAliment("Pollastre", "depen", 8.8);
		
		dj.generateJoguina("Pilota", "si", 2);
		dj.generateJoguina("hotwheels", "no", 4);
		
		Tamagochi t2 = dt.find(2);
		Tamagochi t3 = dt.find(3);
		Tamagochi t4 = dt.find(4);
		
		
		Aliment a1 = da.find(1);
		Aliment a2 = da.find(2);
		Aliment a3 = da.find(3);
		Aliment a4 = da.find(4);

		Joguina j1 = dj.find(1);
		Joguina j2 = dj.find(2);
		
		dt.equipar(t1, a1);
		
		System.out.println(t1);
		System.out.println(a1);
		
		dt.equipar(t1, j1);
		dt.equipar(t2, a1);
		dt.equipar(t2, j2);

		da.listTamagochis(a1.getId());
		
		System.out.println(t1);
		dt.alimentarse(t1);
		System.out.println(t1);
		
		
		
		
		dt.ferseAmics(t1, t2);
		dt.ferseAmics(t2, t4);
		dt.ferseAmics(t4, t3);
		
		List<Tamagochi> tamagochis = dt.findAll();
		System.out.println("TOTS ELS TAMAGOCHIS");
		tamagochis.forEach(t -> System.out.println("- " + t.toCoolString()));

		Utils.close();

	}
}
