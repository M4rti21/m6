# Apunts Laravel
## Index
- [Start](#start)
   * [installation](#installation)
   * [commands](#commands)
   * [file structure](#file-structure)
- [Examples](#examples)
   * [Routes](#routes)
   * [Controllers](#controllers)
   * [Models](#models)
   * [Migrations](#migrations)
   * [Database](#database)
   * [Populate a database](#populate-a-database)
   * [Forms](#forms)

<a name="start"></a>
# Start
<a name="installation"></a>
## Installation
```sh
composer install
npm install
npm run dev
php artisan serve
```
<a name="commands"></a>
## Commands
```sh
php artisan make:controller NomDelController
php artisan make:view NomDeLaView
php artisan make:model NomDelModel
php artisan make:factory NomDeLaFactory
# create db schemes
php artisan migrate:fresh

# populate the db
php artisan make:seeder NomDelSeeder
php artisan db:seed
```
<a name="file-structure"></a>
## File structure
```
/routes/web.php
/app/Http/Controllers/SomeController.php
/app/Models/ExampleModel.php
/resources/views/SomeView.blade.php
/database/migrations/0000_00_0_000000_create_example_table.php
/database/seeders/ExampleSeeder.php
/database/factories/ExampleFactory.php
```
<a name="examples"></a>
# Examples
<a name="routes"></a>
## Routes
<a name="routeswebphp"></a>
##### /routes/web.php
```php
// Simple route
Route::get('/test', function () {
    return "hello";
});

// Simple route with params
Route::get('/test/{name}', function ($name) {
    return "hello".$name;
}); // or

// Handle route from controller
Route::get('/auth/get', [Controller1::class, 'show']);

// Protect route with middleware
Route::get('/auth/get', [Controller1::class, 'show'])->middleware(['auth', 'verified'])->name('routeName');
```
<a name="controllers"></a>
## Controllers
<a name="routeswebphp-1"></a>
##### /routes/web.php
```php
Route::get('/param/{id}/{user}', [UserController::class, 'show']);
```
<a name="apphttpcontrollersusercontrollerphp"></a>
##### /app/Http/Controllers/UserController.php
```php
class UserController extends Controller {
    public function show(Request $req): View {
        return view('userpage')->with('id', $req->id)->with('user', $req->user);
    }
}
```
<a name="models"></a>
## Models
<a name="appmodelsuserphp"></a>
##### /app/Models/User.php
```php
class User extends Authenticatable {
    public $timestamps = false;
    
    // NORMAL FIELDS
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'address'
    ];
    
    // RELATIONAL FIELDS
    public function posts(): HasMany {
        return $this->hasMany(Post::class);
    }
    public function favourite_posts(): BelongsToMany {
        return $this->belongsToMany(Post::class);
    }

    // HIDDEN FIELDS (serialized)
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // CASTED FIELDS (idk)
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
}
```
<a name="appmodelspostphp"></a>
##### /app/Models/Post.php
```php
class Post extends Model {
    protected $fillable = [
        'title',
        'description'
    ];
    
    public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function comments(): HasMany {
        return $this->hasMany(Comment::class);
    }

    public function favourited_by(): BelongsToMany {
        return $this->belongsToMany(User::class);
    }
}
```
<a name="migrations"></a>
## Migrations
<a name="databasemigrations2024_03_11_160542_create_user_tablephp"></a>
##### /database/migrations/2024_03_11_160542_create_user_table.php
```php
return new class extends Migration {  
	public function up(): void  {  
		Schema::create('users', function (Blueprint $table) {  
			$table->id();  
			$table->string('name');  
			$table->string('email')->nullable()->unique();  
			$table->timestamp('email_verified_at')->nullable();  
			$table->string('password')->nullable();  
			$table->string('phone')->nullable();  
			$table->string('address')->nullable();  
			$table->rememberToken();  
		});  
	} 
	public function down(): void {  
		Schema::dropIfExists('users');  
	}  
};
```
<a name="databasemigrations2024_03_11_160622_create_post_tablephp"></a>
##### /database/migrations/2024_03_11_160622_create_post_table.php
```php
return new class extends Migration {   
	public function up(): void {  
		Schema::create('posts', function (Blueprint $table) {
			$table->id();
			$table->string('title');
			$table->string('description');
			$table->timestamps();
			$table->foreignId('user_id')->nullable()->constrained(table: 'users');             
		});
	}
	public function down(): void {  
	  Schema::dropIfExists('post');
  }
};
```
<a name="databasemigrations2024_03_11_165619_create_user_favouriteposts_tablephp"></a>
##### /database/migrations/2024_03_11_165619_create_user_favouriteposts_table.php
```php
return new class extends Migration {  
	public function up(): void {  
		Schema::create('user_favouriteposts', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->foreignId('user_id')->constrained('users', 'id');
			$table->foreignId('post_id')->constrained('posts', 'id');  
		});
	}
	public function down(): void {
		Schema::dropIfExists('user_favouriteposts');
	}
};
```
<a name="database"></a>
## Database
```php
public function sqlExamples($name){
 //find by id
 $res1 = User::find(1);
 //find by id with condition
 $res2 = User::where('username', $name)->get();
 $res3 = User::where('username', $name)->first();
 //find with conditions and order
 $res4 = User::where('age', '>=', 5 )->orderBy('age', 'desc')->first();
 //find all
 $res3 = User::all();
 //update name
 $res1->title = 'new title';
 $res1->save();
 //delete
 $res1->delete();
 }
```
<a name="populate-a-database"></a>
## Populate a database
<a name="userseederphp-extends-seeder"></a>
##### /UserSeeder.php extends Seeder
```php
class UserSeeder extends Seeder {
	public function run(): void {
	    DB::table('Users')->insert([
	        'name' => 'Marti',
	        'age' => 19,
	    ])
	    DB::table('Users')->insert([
	        'name' => 'Roger',
	        'age' => 16,
	    ])
	    DB::table('Users')->insert([
	        'name' => 'Arlet',
	        'name' => 10,
	    ])
	}
}
```
<a name="databaseseedersdatabaseseederphp"></a>
##### /database/seeders/DatabaseSeeder.php
```php
class DatabaseSeeder extends Seeder {
	public function run():void {
	    $this->call([
		    UserSeeder::class,
		    ExampleSeeder::class,
		    //...
	    ]);
	}
}
```
<a name="forms"></a>
## Forms
<a name="resourcesviewsformphp"></a>
##### /resources/views/form.blade.php
```html
<form action="{{route('routeName')}}" method="POST">
	@csrf
	@method('POST')
	<input type="text" name="username" />
	<button type="submit">Send</button>
</form>
```
<a name="routeswebphp-2"></a>
##### /routes/web.php
```php
Route::post('/form', [UserController::class, 'helloForm'])->middleware(['auth', 'verified'])->name('routeName');
```
<a name="apphttpcontrollersusercontrollerphp-1"></a>
##### /app/Http/Controllers/UserController.php
```php
class UserController extends Controller {
    public function helloForm(Request $req): View {
        return view("userPage")->with("username", $req->username);
    }
}
```
<a name="resourcesviewsuserpagebladephp"></a>
##### /resources/views/userPage.blade.php
```html
<div>Hi there, {{$username}}!</div>
```