<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;

class CarController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    // Mostra la pàgina principal amb totes les ofertes de cotxes.
    public function index(): Collection
    {
        return Car::all();
        //return view();

    }
//    public function suma($valor1=null, $valor2=null)
//    {
//        if($valor1===null || $valor2===null)
//            return "qué vols sumar??????????";
//        return $valor1+$valor2;
//    }

}
