<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Post::create([
            'title' => 'Knight Rider',
            'description' => 'Este es el contenido del post 1',
            'user_id' => 3
        ]);
        Post::create([
            'title' => 'Delorean',
            'description' => 'Este es el contenido del post 2',
            'user_id' => 2
        ]);
        Post::create([
            'title' => 'SuperFerrari',
            'description' => 'Este es el contenido del post 3',
            'user_id' => 1
        ]);
        Post::create([
            'title' => 'Misterymachine',
            'description' => 'Este es el contenido del post 3',
            'user_id' => 2
        ]);
    }
}
