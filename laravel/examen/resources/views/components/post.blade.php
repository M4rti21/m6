<div class="card w-96 bg-base-100 shadow-xl mt-5 mt-5">
    <img class="h-80 rounded-t-lg object-cover" src="{{$post->car->image}}" alt="Shoes"
         onerror="this.src='https://www.oeschs-die-dritten.ch/wp-content/uploads/2019/01/D5_Oesch_172-e1548615354982.jpg'"/>
    <div class="card-body">
        <h2 class="card-title">
            {{$post->title}}
            <div class="badge badge-success">{{number_format($post->car->price)}}$</div>
        </h2>
        <p>{{$post->description}}</p>
        <div class="card-actions justify-between">

            <a href="/posts/{{$post->id}}" class="btn btn-info">
                Open Post
            </a>

            <form method="post" action="{{$button_url}}">
                @csrf
                <button type="submit" class="btn" name="post_id" value="{{$post->id}}">
                    {{$button_text}}
                </button>
            </form>
        </div>
    </div>
</div>
