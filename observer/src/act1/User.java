package act1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class User implements PropertyChangeListener {
	private String username;
	
	public String getUsername() {
		return username;
	}

	public User(String username) {
		super();
		this.username = username;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (((Message)evt.getNewValue()).getReciever().getUsername().equals(this.username)) {
			Message m = (Message)evt.getNewValue();
			System.out.println(this.username + ": (Message from " + m.getSender().getUsername() + ") - " + m.getContent());
		}
	}
}
