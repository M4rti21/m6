package act1;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class MessageSender {

	private PropertyChangeSupport support;
	private Message msg;
	
	public MessageSender() {
		this.support = new PropertyChangeSupport(this);
	}
	
	public void addUser(User u) {
		this.support.addPropertyChangeListener(u);
	}
	
	public void delUser(User u) {
		this.support.removePropertyChangeListener(u);
	}
	
	public void setMsg(Message m) {
		this.support.firePropertyChange("msg", this.msg, m);
		this.msg = m;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		this.support.addPropertyChangeListener(pcl);
	}
	
	public void delPropertyChangeListener(PropertyChangeListener pcl) {
		this.support.removePropertyChangeListener(pcl);
	}
	
}
