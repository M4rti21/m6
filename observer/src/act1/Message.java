package act1;

public class Message {
	private User sender;
	private User reciever;
	private String content;

	public Message(User sender, User reciever, String content) {
		super();
		this.sender = sender;
		this.reciever = reciever;
		this.content = content;
	}

	public User getSender() {
		return sender;
	}

	public User getReciever() {
		return reciever;
	}

	public String getContent() {
		return content;
	}

}
