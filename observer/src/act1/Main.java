package act1;

public class Main {
	public static void main(String[] args) {
		User u1 = new User("Marti");
		User u2 = new User("Roger");
		User u3 = new User("Arlet");
		
		MessageSender sys = new MessageSender();
		sys.addUser(u1);
		sys.addUser(u2);
		sys.addUser(u3);
		
		sys.setMsg(new Message(u1, u2, "Hola"));
		sys.setMsg(new Message(u2, u3, "Adeu"));
		sys.setMsg(new Message(u3, u1, "Que tal?"));
	}
}
