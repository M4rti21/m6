package Models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tamagochis")
public class Tamagochi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String descripcio;
	private double gana;
	private boolean viu;
	private int felicitat;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Etapa etapa;
	private LocalDateTime data_naixement;

	public int getId() {
		return id;
	}

	public Tamagochi() {
	}

	public Tamagochi(String nom, String descripcio, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
	}

	public Tamagochi(int id, String nom, String descripcio, double gana, boolean viu, int felicitat, Etapa etapa,
			LocalDateTime data_naixement) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.data_naixement = data_naixement;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getData_naixement() {
		return data_naixement;
	}

	public void setData_naixement(LocalDateTime data_naixement) {
		this.data_naixement = data_naixement;
	}

	@Override
	public String toString() {
		return "Tamagochi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu=" + viu
				+ ", felicitat=" + felicitat + ", etapa=" + etapa + ", data_naixement=" + data_naixement + "]";
	}

}