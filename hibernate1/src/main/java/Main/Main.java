package Main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Models.Etapa;
import Models.Tamagochi;

public class Main {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		try {
			if (sessionFactory == null) {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
						.configure("hibernate.cfg.xml").build();
				Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String[] args) {
		//OPEN
		session = getSessionFactory().openSession();
		session.beginTransaction();

		Tamagochi t1 = new Tamagochi("Marti", "Hola", Etapa.Baby);
		Tamagochi t2 = new Tamagochi("Roger", "Adeu", Etapa.Baby);
		Tamagochi t3 = new Tamagochi("Arlet", "Qtal", Etapa.Baby);

		//INSERT
		session.persist(t1);
		session.persist(t2);
		session.persist(t3);
		//SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		Tamagochi db_t1 = session.find(Tamagochi.class, 1);
		System.out.println(db_t1);
		db_t1.setNom("Updategotchi");

		//UPDATE
		session.merge(db_t1);
		
		//SAVE
		session.getTransaction().commit();
		session.getTransaction().begin();

		//GET ALL
		List<Tamagochi> ts = session.createQuery("from Tamagochi").getResultList();
		System.out.println(ts);

		//DELETE
		session.remove(db_t1);
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		//CLOSE
		session.getTransaction().commit();
		session.close();
	}
}
